# Changelog

All notable changes to this project will be documented in this file.

Spade is currently unstable and all 0.x releases are expected to contain
breaking changes. Releases are mainly symbolic and are done on a six-week
release cycle. Every six weeks, the current master branch is tagged and
released as a new version.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

### Added

### Fixed

- [!84][!84]: The command `swim update-spade` has been fixed so it actually works.
- [!87][!87]: Stop rebuliding spade-python twice if changes occurred

### Changed

### Removed

### Internal

[!84]: https://gitlab.com/spade-lang/swim/-/merge_requests/84
[!87]: https://gitlab.com/spade-lang/swim/-/merge_requests/87

## [0.2.0] - 2023-04-20

- [!82][!82]*Breaking change*: Stop including stdlib and prelude in spade builds.

[!82]: https://gitlab.com/spade-lang/swim/-/merge_requests/82

## [0.1.0] - 2023-03-07

Initial numbered version

[Unreleased]: https://gitlab.com/spade-lang/swim/-/compare/v0.2.0...master
[0.2.0]: https://gitlab.com/spade-lang/swim/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/spade-lang/swim/-/tree/v0.1.0
