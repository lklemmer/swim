use std::process::Command;

use camino::{Utf8Path, Utf8PathBuf};
use color_eyre::eyre::{anyhow, Context, Result};
use log::{info, warn};

use crate::{
    build_dir,
    cmdline::Args,
    config::{Config, LogOutputLevel, NextPnrArgs},
    nextpnr::NextpnrDevice,
    plugin::PluginList,
    report::{FMax, PnrReport, Utilization},
    synth::synthesise,
    util::{make_relative, needs_rebuild, LineAligner},
};

pub enum PnrResult {
    Asc(Utf8PathBuf),
    Textcfg(Utf8PathBuf),
}

impl PnrResult {
    pub fn file(&self) -> &Utf8Path {
        match &self {
            PnrResult::Asc(file) => file,
            PnrResult::Textcfg(file) => file,
        }
    }
}

pub enum PnrTool {
    NextPnr {
        pnr_binary: Utf8PathBuf,
        args: NextPnrArgs<Box<dyn NextpnrDevice>>,
        output: PnrResult,
    },
}

/// Run place and route on the source files.
pub fn run_pnr(
    root_dir: &Utf8Path,
    compiler: &Utf8Path,
    args: &Args,
    config: &Config,
    plugins: &PluginList,
    gui: bool,
) -> Result<PnrResult> {
    let json = synthesise(root_dir, compiler, args, config, plugins, &None)?;
    let report = build_dir(root_dir).join("pnr-timing-report.json");

    let result = match &config.pnr_config()?.pnr_tool()? {
        PnrTool::NextPnr {
            pnr_binary,
            args:
                NextPnrArgs {
                    device,
                    package,
                    allow_unconstrained,
                    allow_timing_fail,
                    pin_file_arg,
                    pin_file,
                },
            output,
        } => {
            // The file that is passed to later stages.
            let actual_target = build_dir(root_dir).join(output.file());
            let output = match output {
                PnrResult::Asc(_) => PnrResult::Asc(actual_target.clone()),
                PnrResult::Textcfg(_) => PnrResult::Textcfg(actual_target.clone()),
            };
            // The file we write in-progress pnr to. This is copied to `actual_target` only if pnr succeeds.
            let wip_target = match actual_target.file_name() {
                Some(file_name) => actual_target.with_file_name(format!("temp-{}", file_name)),
                None => actual_target.with_file_name("temp"),
            };

            if needs_rebuild(&actual_target, [&json, &pin_file])? {
                info!("Running place and route");

                let log = build_dir(root_dir).join("pnr.log");
                let mut command = Command::new(pnr_binary);

                if matches!(config.log_output, LogOutputLevel::Minimal) {
                    let pnr_log = std::fs::File::create(&log)
                        .with_context(|| "Failed to create place and route log output")?;
                    command.stderr(pnr_log);
                }

                command
                    .arg(device.package_flag())
                    .arg("--package")
                    .arg(package)
                    .arg("--json")
                    .arg(make_relative(&json))
                    .arg(pin_file_arg)
                    .arg(make_relative(pin_file));

                match output {
                    PnrResult::Asc(_) => command.arg("--asc").arg(&wip_target),
                    PnrResult::Textcfg(_) => command.arg("--textcfg").arg(&wip_target),
                };

                let status = command
                    .arg("--report")
                    .arg(&report)
                    // lifeguard: https://github.com/YosysHQ/nextpnr/issues/988
                    // .arg("--detailed-timing-report")
                    .args(if *allow_unconstrained {
                        match config.pnr_config()?.as_ref() {
                            crate::config::Pnr::Ice40(_) => {
                                vec!["--pcf-allow-unconstrained"]
                            }
                            crate::config::Pnr::Ecp5(_) => {
                                vec!["--lpf-allow-unconstrained"]
                            }
                        }
                    } else {
                        vec![]
                    })
                    .args(if *allow_timing_fail {
                        vec!["--timing-allow-fail"]
                    } else {
                        vec![]
                    })
                    .args(if gui { vec!["--gui"] } else { vec![] })
                    .status()
                    .context(format!("Failed to run {pnr_binary}"))?;

                if status.success() {
                    std::fs::copy(wip_target, actual_target)?;
                    info!("Finished place and route");
                    output
                } else {
                    let err_msg = format!(
                        "Place and route failed{}",
                        if matches!(config.log_output, LogOutputLevel::Minimal) {
                            format!(". See {} for information", log)
                        } else {
                            String::new()
                        }
                    );
                    return Err(anyhow!(err_msg));
                }
            } else {
                info!("{} is up to date", actual_target);
                output
            }
        }
    };

    let report: Result<PnrReport> = std::fs::read_to_string(&report)
        .with_context(|| format!("Failed to read {}", report))
        .and_then(|s| serde_json::from_str(&s).map_err(|e| anyhow!(e)));

    match report {
        Ok(report) => {
            let frequencies: Vec<String> = report
                .fmax
                .iter()
                .map(
                    |(
                        name,
                        FMax {
                            achieved,
                            constraint,
                        },
                    )| {
                        format!("{name}: {achieved:.1} MHz (target: {constraint} MHz)")
                    },
                )
                .collect();
            info!(
                "Place and route maximum frequencies:\n{}",
                frequencies.join("\n")
            );

            let utilizations: Vec<(String, String)> = report
                .utilization
                .iter()
                .map(|(name, Utilization { available, used })| {
                    (
                        format!("{name}: {used}/{available}"),
                        format!("({:.1}%)", (100f32 * *used as f32 / *available as f32)),
                    )
                })
                .collect();
            let (width_left, width_right) = utilizations.iter().fold(
                (0, 0),
                |(cur_left, cur_right), (next_left, next_right)| {
                    (
                        cur_left.max(next_left.len()),
                        cur_right.max(next_right.len()),
                    )
                },
            );

            let utilization_lines: Vec<String> = utilizations
                .into_iter()
                .map(|(left, right)| {
                    format!(
                        "{}",
                        LineAligner {
                            left,
                            right,
                            width_left,
                            width_right
                        }
                    )
                })
                .collect();
            info!(
                "Place and route components:\n{}",
                utilization_lines.join("\n")
            );
        }
        Err(e) => {
            warn!("Error parsing pnr report: {}", e);
        }
    }

    Ok(result)
}
