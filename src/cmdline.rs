use camino::Utf8PathBuf;
use clap::Parser;

#[derive(Parser, Debug)]
pub struct SimulationArgs {
    /// Do not translate the VCD file to add spade types after the tests are done
    #[clap(long)]
    pub skip_translation: bool,
    /// Only run testbenches whose file name contains the specified string
    pub testbench_filter: Option<String>,
    /// Only run test cases in the specified list.
    #[clap(short, long)]
    pub testcases: Vec<String>,
}

#[derive(Parser, Debug)]
pub enum Command {
    #[clap(visible_alias = "b")]
    Build,
    #[clap(visible_alias = "syn")]
    Synth {
        /// Read the yosys commands from this file instead of running `synth_<architecture>`. Before
        /// the commands are run, read_verilog and hierarchy are run automatically to include the
        /// project verilog files and set the top module
        #[clap(long)]
        yosys_command_file: Option<String>,
    },
    #[clap(visible_alias = "p")]
    Pnr {
        #[clap(short, long)]
        gui: bool,
    },
    #[clap(visible_alias = "u")]
    Upload,
    #[clap(visible_alias = "sim", visible_alias = "test", visible_alias = "t")]
    Simulate(SimulationArgs),
    /// Initialise a new swim project in the specified directory. Optionally copies from a template
    Init {
        #[clap(
            long,
            default_value = "https://gitlab.com/spade-lang/swim-templates.git"
        )]
        template_repo: String,
        #[clap(long)]
        board: Option<String>,
        #[clap(long)]
        list_boards: bool,
        dir: Option<Utf8PathBuf>,
    },
    /// Updates all external dependencies that either have a set branch or tag, or hasn't been
    /// downloaded locally.
    Update,
    #[clap(name = "update-spade")]
    UpdateSpade,
    /// Restore (discard) changes made to git-dependencies (including the compiler).
    Restore,
    Clean,
}

impl Command {
    /// Whether the command requires a swim.toml to be present in order to be run.
    pub fn requires_swim_toml(&self) -> bool {
        match self {
            Command::Build
            | Command::Synth { .. }
            | Command::Pnr { .. }
            | Command::Upload
            | Command::Simulate(_)
            | Command::Update
            | Command::UpdateSpade
            | Command::Restore
            | Command::Clean => true,
            Command::Init { .. } => false,
        }
    }

    #[cfg(test)]
    pub(crate) fn into_args(self) -> Args {
        Args::with_command(self)
    }
}

/// The spade build tool
#[derive(Parser, Debug)]
#[clap(version=env!("VERSION"), about)]
pub struct Args {
    #[clap(subcommand)]
    pub command: Command,

    /// Tell the tools which support it to use quiet output
    #[clap(short = 'q', long)]
    pub quiet: bool,

    /// Run spadec in gdb
    #[clap(long)]
    pub debug_spadec: bool,

    /// Use a spade compiler at this path instead of the one specified in swim.toml
    #[clap(long)]
    pub override_compiler: Option<Utf8PathBuf>,
}

impl Args {
    #[cfg(test)]
    pub fn with_command(command: Command) -> Self {
        Self {
            command,
            quiet: true,
            debug_spadec: false,
            override_compiler: None,
        }
    }
}
