pub fn parse_stat_command(s: &str) -> String {
    // parses output of the form:
    /*
     * === top ===
     *
     *    Number of wires:                 38
     *    Number of wire bits:             53
     *    Number of public wires:          38
     *    Number of public wire bits:      53
     *    Number of memories:               0
     *    Number of memory bits:            0
     *    Number of processes:              0
     *    Number of cells:                 11
     *      SB_DFF                          2
     *      SB_DFFESR                       2
     *      SB_LUT4                         7
     */
    let lines: Vec<&str> = s.lines().collect();

    let first_with_number = lines
        .iter()
        .enumerate()
        .find_map(|(l, line)| line.trim().starts_with("Number").then(|| l));

    let last_with_text = lines
        .iter()
        .enumerate()
        .rev()
        .find_map(|(l, line)| line.trim().is_empty().then(|| l))
        .unwrap_or(lines.len() - 1);

    let whitespace_to_trim = first_with_number
        .and_then(|first_with_number| lines[first_with_number].find(|c: char| !c.is_whitespace()))
        .unwrap_or(0);
    let lines: Vec<&str> = lines
        .iter()
        .take(last_with_text)
        .skip(first_with_number.unwrap_or(0))
        .map(|line| &line[whitespace_to_trim..])
        .collect();
    lines.join("\n")
}
