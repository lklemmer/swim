/**
 The build tool for the Spade programming language

 For command line parameters, run `swim --help`

 For documentation of the swim.toml format, see [crate::config]

 For documentation of the swim_plugin.toml format, see [crate::plugin::config]
*/
use std::{collections::HashMap, process::Command};

use camino::{Utf8Path, Utf8PathBuf};
use clap::Parser;
use color_eyre::{
    eyre::{anyhow, bail, Context},
    Result,
};
use colored::Colorize;
use fern::colors::{Color, ColoredLevelConfig};
use log::{debug, info};

use swim::cmdline::{self, Args};
use swim::config::Config;
use swim::init;
use swim::libraries::{restore_libraries, update_libraries, LockFile, RestoreAction};
use swim::plugin::{load_plugins, restore_plugins};
use swim::pnr::run_pnr;
use swim::simulation::{simulate, translate_vcds, SimulationResult};
use swim::spade::{
    build_spade, build_spade_cxx, build_spadec, build_vcd_translate, restore_spade_repository,
    update_spade,
};
use swim::synth::synthesise;
use swim::upload::upload;
use swim::{libs_dir, lock_file, logfile_path};

fn builtin_command(root_dir: &Utf8Path, args: Args) -> Result<()> {
    debug!("Built-in command {args:?}");

    let prepare_normal_flow = || -> Result<_> {
        let config = Config::read(root_dir, &args.override_compiler)?;

        let binaries = build_spadec(root_dir, &args, &config)?;
        let plugins = load_plugins(root_dir, &config, RestoreAction::Deny)?;
        if plugins.inner.iter().any(|(_, p)| p.requires_cxx) {
            build_spade_cxx(root_dir, &config)?;
        }
        plugins.run_preprocessing_commands()?;
        Ok((config, binaries, plugins))
    };

    match &args.command {
        cmdline::Command::Build => {
            let (config, binaries, plugins) = prepare_normal_flow()?;
            build_spade(root_dir, &binaries, &args, &config, &plugins)?;
        }
        cmdline::Command::Synth { yosys_command_file } => {
            let (config, binaries, plugins) = prepare_normal_flow()?;
            synthesise(
                root_dir,
                &binaries,
                &args,
                &config,
                &plugins,
                yosys_command_file,
            )?;
        }
        cmdline::Command::Pnr { gui } => {
            let (config, binaries, plugins) = prepare_normal_flow()?;
            run_pnr(root_dir, &binaries, &args, &config, &plugins, *gui)?;
        }
        cmdline::Command::Upload => {
            let (config, binaries, plugins) = prepare_normal_flow()?;
            upload(root_dir, &binaries, &args, &config, &plugins)?;
        }
        cmdline::Command::Simulate(sim_args) => {
            let (config, binaries, plugins) = prepare_normal_flow()?;
            let SimulationResult { type_file, result } =
                simulate(root_dir, &binaries, &args, sim_args, &config, &plugins)?;

            let vcd_translator = build_vcd_translate(root_dir, &args, &config)?;
            let translated_vcds = if !sim_args.skip_translation {
                let vcd_files = result.iter().map(|r| r.vcd_file.clone()).collect();
                translate_vcds(&vcd_translator, vcd_files, type_file)
                    .context("Failed to translate VCD files")?
            } else {
                HashMap::new()
            };

            let mut num_fails = 0;
            for file in result {
                if !file.failed_tests.is_empty() {
                    let vcd = translated_vcds
                        .get(&file.vcd_file)
                        .cloned()
                        .flatten()
                        .map(|p| p.to_string())
                        .unwrap_or_else(|| file.vcd_file.to_string());
                    println!("{}: {} [{vcd}]", file.file, "FAIL".red());
                    println!("\tFailed test cases:");
                    for case in file.failed_tests {
                        println!("\t{}", case.name);
                        num_fails += 1;
                    }
                } else {
                    println!("{}: {}", file.file, "PASS".green())
                }
            }

            if num_fails != 0 {
                bail!("{num_fails} test cases failed");
            }
        }
        cmdline::Command::Init {
            dir,
            template_repo,
            board,
            list_boards,
        } => {
            if *list_boards {
                init::list_boards(template_repo)?;
            } else {
                init::init(
                    dir.as_ref().map(|dir| dir.as_path()),
                    template_repo,
                    board.as_deref(),
                )?;
            }
        }
        cmdline::Command::Update => {
            let config = Config::read(root_dir, &args.override_compiler)?;
            let mut lock_file = LockFile::open_or_default(lock_file(root_dir));
            update_libraries(&libs_dir(root_dir), &config, &mut lock_file)?;
            if let Some(libraries) = &mut lock_file.inner.libraries {
                libraries.filter_unseen();
            }
            lock_file.try_write()?;
        }
        cmdline::Command::UpdateSpade => {
            let config = Config::read(root_dir, &args.override_compiler)?;
            update_spade(root_dir, &config)?;
        }
        cmdline::Command::Restore => {
            let config = Config::read(root_dir, &args.override_compiler)?;
            let mut lock_file = LockFile::open_or_default(lock_file(root_dir));
            restore_libraries(root_dir, &config, &mut lock_file)?;
            restore_plugins(root_dir, &config, &mut lock_file)?;
            restore_spade_repository(root_dir, &config)?;
            if let Some(libraries) = &mut lock_file.inner.libraries {
                libraries.filter_unseen();
            }
            lock_file.try_write()?;
        }
        cmdline::Command::Clean => {
            if Utf8PathBuf::from("swim.toml").exists() {
                let build_dir = swim::build_dir(root_dir);
                if build_dir.exists() {
                    info!("Removing build directory");
                    std::fs::remove_dir_all(build_dir)
                        .context("Failed to remove build directory")?;
                } else {
                    info!("Project is already clean");
                }
            } else {
                return Err(anyhow!("Did not find swim.toml"));
            }
        }
    }

    Ok(())
}

/// Try to parse and run an external command with `swim-`-prefix.
///
/// For example, calling `swim lifeguard` will try to execute `swim-lifeguard`
/// with the rest of the arguments passed.
///
/// The outer [Result] represents if the command was run or not. The inner
/// [Result] represents whether the command exited with a zero exit status or not.
fn external_command() -> Result<Result<()>> {
    let mut args = std::env::args();
    let _ = args.next(); // `swim` or whatever our binary is named.
    let command = args.next().ok_or(anyhow!("no command"))?;

    let status = Command::new(format!("swim-{}", command))
        .arg(command)
        .args(args)
        .status()?;

    if status.success() {
        Ok(Ok(()))
    } else {
        Ok(Err(anyhow!("non-zero exit status")))
    }
}

fn setup_logging<'p>(logfile: impl Into<Option<&'p Utf8Path>>) -> Result<()> {
    let colors = ColoredLevelConfig::new()
        .error(Color::Red)
        .warn(Color::Yellow)
        .info(Color::Green)
        .debug(Color::Blue)
        .trace(Color::White);

    let file_config = if let Some(logfile) = logfile.into() {
        std::fs::create_dir_all(
            logfile
                .parent()
                .expect("swim.log path has no parent directory"),
        )?;
        Some(
            fern::Dispatch::new()
                .level(log::LevelFilter::Debug)
                .format(move |out, message, record| {
                    out.finish(format_args!(
                        "{}[{}] {}",
                        chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                        record.level(),
                        message
                    ))
                })
                .chain(fern::log_file(logfile)?),
        )
    } else {
        None
    };

    let stdout_config = fern::Dispatch::new()
        .level(log::LevelFilter::Info)
        .format(move |out, message, record| {
            out.finish(format_args!(
                "[{}] {}",
                colors.color(record.level()),
                message
            ))
        })
        .chain(std::io::stdout());

    let dispatch = fern::Dispatch::new().chain(stdout_config);

    let dispatch = if let Some(file_config) = file_config {
        dispatch.chain(file_config)
    } else {
        dispatch
    };
    dispatch.apply()?;

    Ok(())
}

fn main() -> Result<()> {
    color_eyre::install()?;
    let current_dir = Utf8PathBuf::try_from(std::env::current_dir()?)?;
    let args = swim::cmdline::Args::try_parse();

    // We don't want to create a swim.log if the user hasn't "opted into swim". We assume that a
    // valid swim.toml means we can go into a build directory and put the log there.

    // Try to read a config without caring about the actual result.
    // NOTE: Let's hope that parsing the config is cheap.
    let has_valid_toml = Config::read(&current_dir, &None).is_ok();

    let logfile = if has_valid_toml {
        Some(logfile_path(&current_dir))
    } else {
        None
    };

    setup_logging(logfile.as_deref())?;

    match args {
        Ok(args) => builtin_command(&current_dir, args),
        Err(e) => {
            // If an external command exists, try to use it.
            match external_command() {
                Ok(inner) => inner,
                // No external command was found so report the original clap error instead.
                Err(_) => e.exit(),
            }
        }
    }
}
